import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final nameController = TextEditingController();
  final surnameController = TextEditingController();
  final mobileController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    StreamBuilder<List<User>>(
        stream: readUser(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text("Something went wrong! ${snapshot.error}");
          } else if (snapshot.hasData) {
            final users = snapshot.data!;
            return ListView(
              children: users.map(buildUser).toList(),
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        });
    final name = TextField(
      obscureText: false,
      controller: nameController,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Enter your FullNames",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0)),
      ),
    );

    final surname = TextField(
      obscureText: false,
      controller: surnameController,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Enter your LastName",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0)),
      ),
    );

    final mobile = TextField(
      obscureText: false,
      controller: mobileController,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Enter your Cell Number",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0)),
      ),
    );

    final email = TextField(
      obscureText: false,
      controller: emailController,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Enter your Email address",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0)),
      ),
    );

    final password = TextField(
      obscureText: true,
      controller: passwordController,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Enter your Password",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0)),
      ),
    );

    final registrationButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(20.0),
      color: Colors.blue[700],
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.fromLTRB(50.0, 15.0, 50.0, 15.0),
        onPressed: () {
          final user = User(
              name: nameController.text,
              surname: surnameController.text,
              mobile: mobileController.text,
              email: emailController.text,
              password: passwordController.text);

          createUser(user);

          showAlertDialog(context, user);
        },
        child: const Text(
          "Submit",
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
    return Scaffold(
      appBar: AppBar(title: const Text("Registration")),
      body: Center(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Registration Form",
                  style: TextStyle(
                      color: Colors.blue[700],
                      fontSize: 30,
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 25.0),
                name,
                const SizedBox(height: 25.0),
                surname,
                const SizedBox(height: 25.0),
                mobile,
                const SizedBox(height: 25.0),
                email,
                const SizedBox(height: 25.0),
                password,
                const SizedBox(height: 25.0),
                registrationButton,
                const SizedBox(height: 25.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        final docUser = FirebaseFirestore.instance
                            .collection("users")
                            .doc("my-id");

                        // Update Specific fields

                        docUser.update({
                          "name": "Majabuza",
                        });
                      },
                      child: const Text("Update"),
                    ),
                    const SizedBox(height: 25.0),
                    ElevatedButton(
                        onPressed: () {
                          final docUser = FirebaseFirestore.instance
                              .collection("users")
                              .doc("my-id");

                          // Delete Specific fields

                          docUser.delete();
                        },
                        child: const Text("Delete")),
                    const SizedBox(height: 25.0),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

showAlertDialog(BuildContext context, User user) {
  Widget okButton = TextButton(
    child: const Text("GOT IT"),
    onPressed: () {
      Navigator.of(context, rootNavigator: true).pop("Alert");
    },
  );

  AlertDialog alert = AlertDialog(
    title: const Text("User Information"),
    content: Text(user.name +
        "\n" +
        user.surname +
        "\n" +
        user.mobile +
        "\n" +
        user.email +
        "\n" +
        user.password),
    actions: [okButton],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

Widget buildUser(User user) => ListTile(
      leading: const CircleAvatar(child: Text("Marvellous")),
      title: Text(user.name),
      subtitle: Text(user.email.toString()),
    );

Stream<List<User>> readUser() =>
    FirebaseFirestore.instance.collection('users').snapshots().map((snapshot) =>
        snapshot.docs.map((doc) => User.fromJson(doc.data())).toList());

Future createUser(User user) async {
// reference to document
  final docUser = FirebaseFirestore.instance.collection('users').doc();
  user.id = docUser.id;
  final json = user.tojson();
  await docUser.set(json);
}

class User {
  //Declaring Variables

  String id;
  final String name;
  final String surname;
  final String mobile;
  final String email;
  final String password;

  User({
    this.id = " ",
    required this.name,
    required this.surname,
    required this.mobile,
    required this.email,
    required this.password,
  });

  static User fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        name: json["name"],
        surname: json["surname"],
        mobile: json["mobile"],
        email: json["email"],
        password: json["password"],
      );

  Map<String, dynamic> tojson() => {
        "id": id,
        "name": name,
        "surname": surname,
        "mobile": mobile,
        "email": email,
        "password": password,
      };

  //Getters
  String get getid {
    return id;
  }

  String get getname {
    return name;
  }

  String get getsurname {
    return surname;
  }

  String get getmobile {
    return mobile;
  }

  String get getemail {
    return email;
  }

  String get getpassword {
    return password;
  }

  //Setters

  //set setId(String id) {
  // id = id;
  //}

  //set setName(String name) {
  //name = name;
  //}

  //set setSurname(String surname) {
  //surname = surname;
  //}

  //set setMobile(String mobile) {
  //mobile = mobile;
  //}

  //set setEmail(String email) {
  //email = email;
  //}

  //set setPassword(String password) {
  //password = password;
  //}
}
