// ignore_for_file: avoid_print

import 'package:flutter/material.dart';

class FormScreen extends StatefulWidget {
  const FormScreen({Key? key}) : super(key: key);

  @override
  State<FormScreen> createState() => _FormScreenState();
}

class _FormScreenState extends State<FormScreen> {
  String _name = " ";
  String _email = " ";
  String _password = " ";
  String _url = " ";
  String _cellnumber = " ";

  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  Widget _buildName() {
    return TextFormField(
      decoration: const InputDecoration(
        icon: Icon(Icons.person),
        hintText: 'Please Enter your name',
        labelText: 'Name *',
      ),
      onSaved: (String? value) {
        _name = " ";
      },
      validator: (String? value) {
        return (value != null && value.contains('@'))
            ? 'Do not use the @ char.'
            : null;
      },
    );
  }

  Widget _buildEmail() {
    return TextFormField(
      decoration: const InputDecoration(
        icon: Icon(Icons.email),
        hintText: 'Please Enter your Email address',
        labelText: 'E-mail *',
      ),
      onSaved: (String? value) {
        _email = " ";
      },
      validator: (String? value) {
        return (value != null && value.contains('#'))
            ? 'Do not use the # char.'
            : null;
      },
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      decoration: const InputDecoration(
        icon: Icon(Icons.password),
        hintText: 'Please Enter your Password',
        labelText: 'E-mail *',
      ),
      onSaved: (String? value) {
        _password = " ";
      },
      validator: (String? value) {
        return (value != null && value.contains(' '))
            ? 'Do not create space'
            : null;
      },
    );
  }

  Widget _buildUrl() {
    return TextFormField(
      decoration: const InputDecoration(
        icon: Icon(Icons.circle),
        hintText: 'Please Enter your Url',
        labelText: 'Url *',
      ),
      onSaved: (String? value) {
        _url = " ";
      },
      validator: (String? value) {
        return (value != null && value.contains(' '))
            ? 'Do not create space'
            : null;
      },
    );
  }

  Widget _buildCellNumber() {
    return TextFormField(
      decoration: const InputDecoration(
        icon: Icon(Icons.phone_android),
        hintText: 'Please Enter your Phone number',
        labelText: 'Phone Number *',
      ),
      onSaved: (String? value) {
        _cellnumber = " ";
      },
      validator: (String? value) {
        return (value != null && value.contains(' '))
            ? 'Do not create space'
            : null;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Form Screen")),
      body: Container(
        margin: const EdgeInsets.all(24),
        child: Form(
            key: _formkey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _buildName(),
                _buildEmail(),
                _buildPassword(),
                _buildUrl(),
                _buildCellNumber(),
                const SizedBox(height: 100),
                TextButton(
                  onPressed: () {
                    if (!_formkey.currentState!.validate()) {
                      return;
                    }
                    _formkey.currentState!.save();

                    print(_name);
                    print(_email);
                    print(_password);
                    print(_url);
                    print(_cellnumber);
                  },
                  child: const Text(
                    "Submit",
                    style: TextStyle(color: Colors.blue, fontSize: 16),
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
